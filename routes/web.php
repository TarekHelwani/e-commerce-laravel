<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
});

Route::view('/register', 'register');
Route::post('/login' , [UserController::class , 'login']);
Route::post('/register' , [UserController::class , 'register']);
Route::get('/' , [ProductController::class , 'index']);
Route::get('/details/{id}' , [ProductController::class , 'details']);
Route::get('/search' , [ProductController::class , 'search']);
Route::post('/add_to_cart', [ProductController::class , 'addToCart']);
Route::get('/logout' , [UserController::class , 'logout']);
Route::get('/cart_list', [ProductController::class , 'showCartList']);
Route::get('/removecart/{id}' , [ProductController::class , 'removeCart']);
Route::get('/order' , [ProductController::class , 'order']);
Route::post('/orderplace' , [ProductController::class , 'orderPlace']);
Route::get('/myorders',[ProductController::class , 'myOrders']);