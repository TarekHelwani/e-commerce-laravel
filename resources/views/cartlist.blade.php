@extends('master')
@section('content')
<div class="custom-product">
    <div class="col-sm-10">
        <div class="trending-wrapper">
            <h4>Result for Products</h4>
            <a href="order" class="btn btn-success">Order Now</a> <br> <br>
            @foreach ($products as $item)
            <div class="searched-item row cart-list-divider">
                <div class="col-sm-3">
                    <a href="details/{{ $item->id }}">
                        <img class="trending-image" src="{{ $item->gallery }}">
                    </a>
                </div>
                <div class="col-sm-4">
                    <div class="">
                        <h2>{{ $item->name}}</h2>
                        <h5>{{ $item->description }}</h5>
                    </div>
                </div>
                <div class="col-sm-3">
                    <a href="/removecart/{{ $item->cart_id }}" class="btn btn-warning">Remove from Cart</a>
                </div>
            </div>
            @endforeach
            <br> <br>
            <a href="order" class="btn btn-success">Order Now</a> <br> <br>
        </div>
    </div>
</div>
@endsection